﻿using System.IO;
using System.Text;

namespace WepSpriteGen
{
	class Program
	{
		const string SpritesSource = "WeaponSprites.txt";
		const string TemplateSource = "WeaponTemplate.txt";
		const int OffsetX = 20, OffsetY = 20; // [Ace] These are the offsets between each icon.
		static int Main(string[] args)
		{
			string raw = string.Empty, template = string.Empty;
			try
			{
				raw = File.ReadAllText(SpritesSource);
				template = File.ReadAllText(TemplateSource);
			}
			catch (FileNotFoundException e)
			{
				System.Console.WriteLine(e.FileName + " is missing from the executable's directory.");
				System.Console.WriteLine("Press any key to exit.");
				System.Console.ReadLine();
				return 1;
			}

			raw = raw.Replace(" ", string.Empty).Replace("\n", string.Empty).Replace("\r", string.Empty);
			string[] categories = raw.Split(';', System.StringSplitOptions.RemoveEmptyEntries);
			
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < categories.Length; ++i)
			{
				string[] sprites = categories[i].Split(',', System.StringSplitOptions.RemoveEmptyEntries);
				for (int j = 0; j < sprites.Length; ++j)
				{
					sb.Append(template
						.Replace("####", sprites[j])
						.Replace("%wox", $"{-OffsetX * j}")
						.Replace("%woy", $"{-OffsetY * i}"));
					
					if (i < categories.Length - 1)
					{
						sb.AppendLine();
					}
				}
			}

			File.WriteAllText("TEXTURES.Weapons.txt", sb.ToString());
			return 0;
		}
	}
}

class RedlineWeapon : BEWeaponBase
{
	override void Tick()
	{
		if (!owner || owner.player.ReadyWeapon != self || Overheated)
		{
			IsRevving = false;
		}

		if (Rpm > MaxRpm * 1.1)
		{
			Rpm = int(Rpm * frandom(0.91, 0.92));
		}

		if (!IsRevving)
		{
			Rpm -= 2;
			Rpm = int(Rpm * 0.995);
		}
		Rpm = int(max(0, Rpm));

		if (Heat > 0)
		{
			if (Overheated)
			{
				Heat--;
			}
			if (A_GetWeaponLevel() >= 2)
			{
				Heat--;
			}
			Heat--;
		}
		else
		{
			Overheated = false;
			Heat = 0;
		}

		if (owner)
		{
			if (Rpm > 0)
			{
				owner.A_StartSound("weapons/redlinerevving", 100, CHANF_LOOPING);
			}
			double RpmFactor = Rpm / double(MaxRpm);
			owner.A_SoundPitch(100, 0.6 + (RpmFactor * 0.7));
			owner.A_SoundVolume(100, min(0.5, RpmFactor * 1.5));
			if (RpmFactor ~== 0)
			{
				owner.A_StopSound(100);
			}
		}

		Super.Tick();
	}

	override void DetachFromOwner()
	{
		owner.A_StopSound(100);
		Super.DetachFromOwner();
	}

	override void OnDestroy()
	{
		if (owner)
		{
			owner.A_StopSound(100);
		}
		Super.OnDestroy();
	}

	/*
	 override void DoEffect()
	 {
	 	if (!owner || !owner.player || owner.player.ReadyWeapon != self)
	 	{
	 		return;
	 	}

	 	Console.Printf("\c[Green]RPM:\c- %i \c[Green]Tier:\c- %i \c[Green]Heat:\c- %i",
	 		Rpm,
	 		A_GetDamageTier(),
	 		Heat);

	 	Super.DoEffect();
	 }
	*/

	override void DrawHUDStuff(BulletEyeHUD hud, BEPlayerBase plr, int flags)
	{
		hud.DrawImage("TRIWYVMT", (-100, -33), flags);
		hud.DrawBar("GRNHLFBR", "GRNHLFBB", Heat, A_GetMaxHeat(), (-106, -33), 0, 0, flags); //Not sure on these offsets, but fix when it's an actual weapon.
		hud.DrawBar("GRNHLHBR", "GRNHLHBB", Rpm, MaxRpm, (-106, -40), 0, 0, flags);

		if (Overheated)
		{
			hud.DrawImage("ABILGRH1", (-140, -15), flags);
		}
	}

	private action void A_CheckOverheat()
	{
		if (invoker.Heat > A_GetMaxHeat())
		{
			invoker.Overheated = true;
			invoker.owner.A_StartSound("weapons/redlineoverload", 10);
		}
	}

	private clearscope action int A_GetDamageTier()
	{
		int Tier = 0;
		for (int i = 0; i < invoker.Notches.Size(); ++i)
		{
			if (invoker.Rpm >= int(MaxRpm * invoker.Notches[i]))
			{
				Tier++;
			}
		}
		return Tier;
	}

	private clearscope action int A_GetMaxHeat(bool raw = false)
	{
		double mult = 1.0;
		return int(300 * mult);
	}

	static const double Notches[] = { 0.3, 0.75 };
	const MaxRpm = 1200;
	const MinDamage = 90;
	const MaxDamage = 120;

	private bool HasFired;
	private bool IsRevving;
	private bool Overheated;
	private int Rpm;
	private int Heat;

	Default
	{
		Inventory.Icon "WEAP33";
		Weapon.SlotNumber 1;
		Weapon.BobStyle "Inverse";
		Weapon.BobSpeed 2.3;
		Weapon.BobRangeX 0.5;
		Weapon.BobRangeY 0.3;
		Weapon.AmmoType1 "AmmoRedline";
		Weapon.AmmoType2 "AmmoTechEnergy";
		Weapon.UpSound "beplayer/weapswitchredline";
		BEWeaponBase.ReloadProperties "weapons/redlinereloadstage1", "weapons/redlinereloadstage2", "weapons/redlinereloadstage3";
		BEWeaponBase.Sprite "WEAPH33";
		BEWeaponBase.Type WTYPE_Tech;
		BEWeaponBase.ExpGainMult 1.0;
		BEWeaponBase.Attributes 5, 3, 1, 4;
		BEWeaponBase.EffectTexts "- Fires a precise beam. Press and hold alt-fire to rev the engine and increase damage dealt. Firing produces heat, and overheating the weapon prevents firing for a short time.", "- Adds a heat sink which increases the heat dissipation rate, allowing you to fire more before overheating.", "";
		BEWeaponBase.FlavorText "";
		BEWeaponBase.Element WElement_Fire;
		BEWeaponBase.CraftingCost 3500;
		+WEAPON.NO_AUTO_SWITCH
		+WEAPON.CHEATNOTWEAPON
		+BEWEAPONBASE.ELITEWEAPON
		Tag "\cl*\cx Redline";
	}

	States
	{
		Spawn:
			PRDL A 0;
			Goto Super::Spawn;
		Ready:
			RDNG A 1
			{
				invoker.IsRevving = false;
				if (!(player.cmd.buttons & BT_ATTACK))
				{
					invoker.HasFired = false;
				}
				A_WeaponReady(WRF_ALLOWRELOAD | WRF_ALLOWZOOM | WRF_ALLOWUSER1 | WRF_ALLOWUSER3 | WRF_ALLOWUSER4 | (invoker.HasFired ? WRF_NOPRIMARY : 0));
			}
			Loop;
		Deselect:
			TNT1 AA 0 A_Lower;
			RDNG A 1 A_Lower;
			Loop;
		Select:
			TNT1 A 0 A_InitOverlays();
			TNT1 AA 0 A_Raise;
			RDNG A 1 A_Raise;
			Loop;
		Fire:
			#### A 1
			{
				if (invoker.Overheated)
				{
					A_StartSound("weapons/redlineoverheated", CHAN_WEAPON);
					return ResolveState('Nope');
				}

				if (CountInv("AmmoRedline") > 0 && !invoker.HasFired)
				{
					invoker.HasFired = true;
					return ResolveState('Shoot');
				}

				return ResolveState(null);
			}
			Goto Ready;
		Nope:
			#### A 1;
			#### A 0 A_ReFire("Nope");
			Goto Ready;
		Shoot:
			#### F 1 Bright Offset(0, 42)
			{
				A_Light0();
				A_StartSound("weapons/redlinefire", CHAN_WEAPON);

				int ChargeDamage = int(random(MinDamage, MaxDamage) ** (1.0 + 0.10 * A_GetDamageTier()));
				A_RailAttack(ChargeDamage, 0, false, "", "", RGF_NOPIERCING | RGF_NORANDOMPUFFZ | RGF_SILENT, 0, "RedlineRayImpact", 0, 0, 12600, 0, 4.0, 0, "RedlineRaySmoke");
				
				A_CheckOverheat();
				invoker.Heat += int(ChargeDamage / 4 * frandom(0.925, 1.05));
				A_TakeAmmo(1);

				A_AlertMonsters();
			}
			#### A 2 Offset(0, 38);
			#### A 1 Offset(0, 34);
			Goto Ready;
		AltFire:
			#### A 1 Offset(0, 34);
			#### B 1 Offset(4, 38);
			#### B 1 Offset(0, 34);
			#### B 0 A_Refire('BoltPull');
			Goto Ready;
		BoltPull:
			#### C 2 Offset(4, 38) A_StartSound("weapons/redlineboltback", 8, CHANF_OVERLAP);
			#### D 1 Offset(6, 42) { invoker.HasFired = false; }
			#### E 2 Offset(6, 42);
			#### E 1 Offset(6, 42) A_WeaponReady(WRF_NOFIRE);
			#### E 0 A_Refire("AltHold");
			Goto AltHoldEnd;
		AltHold:
			#### E 1 A_WeaponReady(WRF_NOFIRE);
			#### E 1
			{
				A_ClearRefire();
				if (CountInv("AmmoRedline") > 0 && !invoker.Overheated)
				{
					invoker.IsRevving = true;
					double heatFac = invoker.Heat / double(A_GetMaxHeat());

					invoker.Rpm += min(40, int((2 ** (invoker.Rpm * (0.0028 - 0.0013 * heatFac)) * (10 + 10 * heatFac))));
				}

				if (player.cmd.buttons & BT_ALTATTACK)
				{
					return ResolveState('AltHold');
				}

				return ResolveState(null);
			}
		AltHoldEnd:
			#### D 2
			{
				A_StartSound("weapons/redlineboltfwd", 8, CHANF_OVERLAP);
				invoker.IsRevving = false;
			}
			#### C 2;
			#### B 2 Offset(2, 36)
			{
				A_Refire("AltHold");
				A_WeaponReady(WRF_NOFIRE);
			}
			Goto Ready;
	}
}

class RedlineRayImpact : BEPlayerPuff
{
	Default
	{
		+NODAMAGETHRUST
		+FORCEDECAL
		+PUFFGETSOWNER
		+ALWAYSPUFF
		+PUFFONACTORS
		+NOINTERACTION
		+BLOODLESSIMPACT
		+FORCERADIUSDMG
		+NOBLOOD
		Decal "RedlineScorch";
		DamageType "Redline";
	}

	States
	{
		Spawn:
			TNT1 A 5 NoDelay
			{
				A_Explode(random(15, 30), 20, XF_HURTSOURCE, false);
				A_StartSound("Redline/Impact");

				for (int i = 0; i < 30; ++i)
				{
					double pitch = frandom(-85.0, 85.0);
					A_SpawnParticle(0xFF1111, SPF_RELATIVE | SPF_FULLBRIGHT, random(10, 20), random(5, 8), random(0, 359), random(0, 4), 0, 0, random(1, 5) * cos(pitch), 0, random(1, 5) * sin(pitch), 0, 0, -0.5);
				}
			}
			Stop;
	}
}

class RedlineRaySmoke : Actor
{
	override void PostBeginPlay()
	{
		if (!random(0, 4))
		{
			A_SpawnItemEx("RedlineRaySmokeParticle");
		}

		A_SetRoll(random(0, 360));

		Super.PostBeginPlay();
	}

	Default
	{
		StencilColor "FF1111";
		RenderStyle "Stencil";
		+NOINTERACTION
		+ROLLSPRITE
		Alpha 2.0;
		Scale 0.005;
	}

	States
	{
		Spawn:
			RDSM K 1 Bright
			{
				A_FadeOut(0.1);
				A_SetScale(Scale.X + 0.0001);
				A_ChangeVelocity(frandom(-0.0025, 0.0025), frandom(-0.0025, 0.0025), frandom(-0.0025, 0.0025), CVF_RELATIVE);
			}
			Loop;
	}
}

class RedlineRaySmokeParticle : Actor
{
	override void PostBeginPlay()
	{
		Lifetime = DefaultLifeTime = random(25, 40);
		ParticleSize = frandom(2.0, 3.0);

		Super.PostBeginPlay();
	}

	Default
	{
		+NOINTERACTION
	}

	double Lifetime;
	double DefaultLifeTime;
	double ParticleSize;

	States
	{
		Spawn:
			TNT1 A 1
			{
				A_SpawnParticle("FF1111", SPF_RELATIVE | SPF_FULLBRIGHT, 1, ParticleSize, startalphaf: Lifetime / DefaultLifeTime);
				A_ChangeVelocity(frandom(-0.10, 0.10), frandom(-0.10, 0.10), frandom(-0.10, 0.10), CVF_RELATIVE);
				if (Lifetime-- < 0)
				{
					Destroy();
				}
			}
			Loop;
	}
}

class AmmoRedline  : Ammo
{
	Default
	{
		Inventory.Amount 30;
		Inventory.MaxAmount 30;
		Inventory.InterHubAmount 30;
	}
}